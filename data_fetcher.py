import requests
import csv


def get_plant_state():
    mimosa_data = "https://docs.google.com/spreadsheet/ccc?key=1L9nYcnQF_HGz9v75ExSZ0GxP7l1fM2wHVYu-H9_ojv0&output=csv"
    r = requests.get(mimosa_data)
    lines = r.text.splitlines()[:10]
    rows = list(csv.reader(lines))
    if '' in rows[1]:
        dictionary = dict(zip(rows[0], rows[2]))
    else:
        dictionary = dict(zip(rows[0], rows[1]))
    return dictionary
