import requests
import json
import os
import sys
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), "..")))
import key

witai_keys = {"api_address": "https://api.wit.ai/",
              "user_token": key.user_token,
              "api_version": "20190211",
              "app_token": key.app_token
              }


def get_predicted_entities(prediction):
    """From Wit.ai prediction, extract entities and their normalization in standard format"""
    formatted_entities = []
    entities = prediction['entities']
    entities.pop('intent', '')
    if entities:
        for entity_tag, entity_list in entities.items():
            for entity in entity_list:
                formatted_entity = dict()
                formatted_entity["tag"] = entity['_entity']
                formatted_entity["value"] = entity['_body']
                formatted_entity["confidence"] = entity["confidence"]
                formatted_entity["start"] = entity['_start']
                formatted_entity["end"] = entity['_end']
                # TODO add all built-in formats
                if entity['_entity'] == 'datetime':
                    if entity['type'] == 'value':
                        formatted_entity['normalization'] = entity['value']
                    elif entity['type'] == 'interval':
                        formatted_entity['normalization'] = dict()
                        formatted_entity['normalization']['from'] = entity['from']
                        formatted_entity['normalization']['to'] = entity['to']
                    formatted_entity['additional_info'] = entity['values']
                elif entity['_entity'] == 'amount_of_money':
                    formatted_entity['normalization'] = dict()
                    formatted_entity['normalization']['value'] = entity['value']
                    formatted_entity['normalization']['currency'] = entity['unit']
                formatted_entities.append(formatted_entity)
        formatted_entities.sort(key=lambda v: v['start'])
    return formatted_entities


def get_predicted_intent(prediction):
    # if no intent was predicted, "intent" is not in the "entities" list
    predicted_intents = prediction['entities'].get('intent', '')
    if predicted_intents:
        return predicted_intents[0]['value']
    else:
        return 'NaN'


def classify_written_sentence(sentence):
    """
    Annotate an input sentence with intent and entities.
    :param sentence: (str) sentence to be annotated
    :param deploy_parameters: (dict) that contains project_id and deploy_id to identify the prediction model
    :return: (dict) an annotated structure of the format:
    {
        "_text": "dimmi quanto ho speso ieri in pizzeria",
        "entities": {
            "datetime": [
                {
                    "confidence": 0.951615,
                    "values": [
                        {
                            "value": "2019-02-11T00:00:00.000+01:00",
                            "grain": "day",
                            "type": "value"
                        }
                    ],
                    "value": "2019-02-11T00:00:00.000+01:00",
                    "grain": "day",
                    "type": "value",
                    "_entity": "datetime",
                    "_body": "ieri",
                    "_start": 22,
                    "_end": 26
                }
            ],
            "CAT": [
                {
                    "confidence": 0.86787903252534,
                    "value": "pizzeria",
                    "type": "value",
                    "_entity": "CAT",
                    "_body": "pizzeria",
                    "_start": 30,
                    "_end": 38
                }
            ],
            "intent": [
                {
                    "confidence": 0.99977217690234,
                    "value": "totale_movimenti_passati",
                    "_entity": "intent"
                }
            ]
        },
        "msg_id": "1e0yYHJ73c3E3kKEV"
    }
    """
    r = requests.get(witai_keys["api_address"] + "message",
                     headers={"Authorization": witai_keys["app_token"], "Content-Type": "application/json",
                              "version": witai_keys["api_version"]},
                     params=(("q", sentence),
                             ("verbose", "true")))
    response = r.json()
    return response


def read_audio(WAVE_FILENAME):
    # function to read audio(wav) file
    with open(WAVE_FILENAME, 'rb') as f:
        audio = f.read()
    return audio


def classify_speech_sentence():
    # get a sample of the audio that we recorded before.
    audio = read_audio("/tmp/voice.mp3")
    # audio = read_audio("/Users/fedja/Downloads/file_0.mp3")

    r = requests.post(witai_keys["api_address"] + "speech",
                     headers={"Authorization": witai_keys["app_token"], "Content-Type": "audio/mpeg3",
                              "version": witai_keys["api_version"]},
                      data=audio)
    #Get the text
    # data = json.loads(r.content)
    # print(data)
    response = r.json()
    return response


if __name__ == "__main__":
    classify_speech_sentence()
