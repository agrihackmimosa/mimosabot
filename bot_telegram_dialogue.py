# -*- coding: utf-8 -*-

import logging
import bot_telegram
from bot_telegram import BOT, send_message, send_messages, send_typing_action, send_text_document, send_message_multi, send_message_query, exception_reporter, report_master
import utility
import telegram
import utility
import time
from conversation import respond_to_request, respond_to_prediction
from nlu.analyzer import classify_speech_sentence
from pydub import AudioSegment
from bot_firestore_user import User

# ================================
# CONFIG
# ================================
DEBUG = False

# ================================
# RESTART
# ================================
def restart_multi(users):
    for u in users:
        redirect_to_state(u, state_INITIAL, message_obj=None)

def restart_user(user):
    redirect_to_state(user, state_INITIAL, message_obj=None)

# ================================
# REDIRECT TO STATE
# ================================
def redirect_to_state_multi(users, new_function, message_obj=None):
    reversed_users = list(reversed(users)) # so that game creator is last
    for u in reversed_users:
        redirect_to_state(u, new_function, message_obj)

def redirect_to_state(user, new_function, message_obj=None):
    new_state = new_function.__name__
    if user.state != new_state:
        logging.debug("In redirect_to_state. current_state:{0}, new_state: {1}".format(str(user.state), str(new_state)))
        user.state = new_state
        user.save()
    repeat_state(user, message_obj)


# ================================
# REPEAT STATE
# ================================
def repeat_state(user, message_obj=None):
    state = user.state
    if state is None:
        restart_user(user)
        return
    method = possibles.get(state)
    if not method:
        msg = "⚠️ User {} sent to unknown method state: {}".format(user.serial_number, state)
        report_master(msg)
    else:
        method(user, message_obj)

# ================================
# Initial State
# ================================
def state_INITIAL(user, message_obj):
    if message_obj is None:
        msg = 'Benvenuto/a in MimosaBot!'
        send_message(user, msg)
    else:
        if message_obj.text:
            text_input = message_obj.text
            response = respond_to_request(text_input)
            send_message(user, response)
        elif message_obj.voice:
            file_id = message_obj.voice.file_id
            newFile = BOT.get_file(file_id)
            newFile.download('/tmp/voice.oga')            
            AudioSegment.from_file("/tmp/voice.oga").export("/tmp/voice.mp3", format="mp3")
            prediction = classify_speech_sentence()
            logging.debug("Prediction: {}".format(prediction))
            response = respond_to_prediction(prediction)
            send_message(user, response)

def deal_with_universal_commands(user, text_input):
    #logging.debug('In universal command with input "{}". User is master: {}'.format(text_input, user.is_master()))
    if text_input == '/start':
        #restart_user(user)
        msg = "Benvenuto/a!"
        restart_user(user)
        return True
    if text_input == '/state':
        s = user.state
        msg = "You are in state {}".format(s)
        send_message(user, msg, markdown=False)
        return True
    if user.is_master():
        if text_input == '/debug':
            msg = "No debug active"
            send_message(user, msg, markdown=False)
            return True
        if text_input == '/test':
            msg = "No test active"
            send_message(user, msg, markdown=False)
            return True
        if text_input == '/exception':
            1/0
            return True
    return False

# ================================
# DEAL WITH REQUEST
# ================================
'''
python-telegram-bot documentation
https://python-telegram-bot.readthedocs.io/en/stable/
'''
@exception_reporter
def deal_with_request(request_json):
    # retrieve the message in JSON and then transform it to Telegram object    
    logging.debug('In deal_with_request.')
    update_obj = telegram.Update.de_json(request_json, BOT)
    logging.debug('update_obj: {}'.format(update_obj))
    message_obj = update_obj.message    
    user_obj = message_obj.from_user
    username = user_obj.username
    last_name = user_obj.last_name if user_obj.last_name else ''
    name = (user_obj.first_name + ' ' + last_name).strip()
    # language = user_obj.language_code

    user = User.get_user('telegram', user_obj.id)
    if user == None:
        user = User.create_user('telegram', user_obj.id, name, username)
        report_master('New user: {} @{}'.format(user.name, user.username))
    else:
        user.update_user(name, username)

    if message_obj:
        logging.debug('message_obj: {}'.format(message_obj))
        if message_obj.text and deal_with_universal_commands(user, message_obj.text):            
            return
        repeat_state(user, message_obj=message_obj)

possibles = globals().copy()
possibles.update(locals())

