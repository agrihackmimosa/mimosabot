from nlu.analyzer import classify_written_sentence, get_predicted_intent, get_predicted_entities
from data_fetcher import get_plant_state

def respond_to_request(text_input):    
    prediction = classify_written_sentence(text_input)
    return respond_to_prediction(prediction)

def respond_to_prediction(prediction):    
    data = get_plant_state() # dictionary
    intent = get_predicted_intent(prediction)
    entities = get_predicted_entities(prediction)
    if intent == 'info_acqua':
        humidity = float(data['humidity'])
        if humidity>300:
            return "L'umidità è {:.1f}. 👍 Ho abbastanza acqua, grazie.".format(humidity)
        else:
            return "L'umidità è {:.1f}. 🥵 Dammi un po' di acqua, grazie.".format(humidity)
    elif intent == 'info_temperatura':
        temperatura = float(data['temperature'])
        if temperatura<10:
            return "La temperatura è di {:.1f}°. 🥶 Ho freddo!".format(temperatura)
        elif temperatura<30:
            return "La temperatura è di {:.1f}°. 👍 sto bene, grazie.".format(temperatura)
        else:
            return "La temperatura è di {:.1f}°. 🥵 Ho caldo, rinfrescami!".format(temperatura)
    elif intent == 'info_generale':
        return "L'umidità è {} e la temperatura è {}".format(data['moisture'], data['temperature'])
    elif intent == 'start_automode':
        if len(entities)>0:
            date = entities[0]['normalization']
            date_str = str(date)[:10]
            return "Ok, mi arrangio da sola fino al {}".format(date_str)
        else:
            return "Ok, mi arrangio da sola!"
    else:
        return "Non riesco a capire cosa mi chiedi!"
