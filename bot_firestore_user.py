import key

import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore

from dataclasses import dataclass, field

# https://gitlab.com/futureprojects/firestore-model/blob/master/examples/main.py
import firestore_model
from firestore_model import Model

# Use the application default credentials
cred = credentials.ApplicationDefault()
firebase_admin.initialize_app(cred, {
  'projectId': key.APP_NAME,
})

firestore_model.db = firestore.client()

@dataclass
class User(Model):
    application: str
    serial_number: str
    name:str
    username:str
    state:str = None

    @staticmethod
    def make_id(application, serial_number):
        return '{}_{}'.format(application, serial_number)

    @staticmethod
    def create_user(application, serial_number, name, username):
        user = User.make(
            application = application,
            serial_number = serial_number,
            name = name,
            username = username
            #save=True
        )
        user.id = User.make_id(application, serial_number)
        user.save()
        return user

    @staticmethod
    def get_user(application, serial_number):
        id_str = User.make_id(application, serial_number)
        return User.get(id_str)

    def update_user(self, name, username):
        self.name = name
        self.username = username

    def is_master(self):
        return self.serial_number == key.TELEGRAM_BOT_MASTER_ID


